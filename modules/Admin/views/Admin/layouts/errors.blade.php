<!DOCTYPE html>
<html>

@include('Admin.layouts.partials.htmlheader')
<body>
    <div id="app">
        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section>
    </div>
</body>
</html>