<!DOCTYPE html>
<html>

@include('Admin.layouts.partials.htmlheader')
@if (session('warning'))
    <div class="alert alert-warning">
        <ul>
            <li>
            	<i class="glyphicon glyphicon-remove"></i> {{ session('warning') }}
            	<a href="{{session('resend_link')}}">Reenviar correo</a>
            </li>
        </ul>
    </div>
@endif

@yield('content')
</html>