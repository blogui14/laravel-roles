<div class="social-auth-links text-center">
	@if(Social::getConnections()->count() > 0)
	    <p>- OR -</p>
		@foreach(Social::getConnections() as $key => $social)
	    	<a href="{{ url('/auth/'.$key) }}" class="btn btn-block btn-social btn-{{$key}} btn-flat"><i class="fa fa-{{$key}}"></i> {{ trans('adminlte_lang::message.sign'.studly_case($key)) }}</a>
	    @endforeach
	@endif
</div><!-- /.social-auth-links -->