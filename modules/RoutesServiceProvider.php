<?php namespace Modules;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RoutesServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */

    protected $namespace_modules = [
    	'users' => [
    		'frontend' => 'Modules\Users\Src\Controllers\Frontend',
    		'admin' => 'Modules\Users\Src\Controllers\Admin',
    	],
        'roles' => [
            'frontend' => 'Modules\Roles\Src\Controllers\Frontend',
            'admin' => 'Modules\Roles\Src\Controllers\Admin',
        ],
        'admin' => [
            'admin' => 'Modules\Admin\Src\Controllers\Admin',
        ],
    	'new_module' => [
    		'frontend' => '',
    		'adin' => ''
    	]
    ];

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapAdminRoutes();
        $this->mapUsersRoutes();
        $this->mapRolesRoutes();

    }

      /**
     * Define the "admin" routes for the application.
     *
     * These routes are only for admin.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        $module = 'admin';

        //Admin
        Route::prefix($module)
             ->middleware('web')
             ->namespace($this->namespace_modules[$module]['admin'])
             ->group(base_path('Modules/Admin/routes/admin.php'));

    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes are only for admin.
     *
     * @return void
     */
    protected function mapUsersRoutes()
    {
    	$module = 'users';

    	//Frontend
        Route::prefix('users')
             ->middleware('web')
             ->namespace($this->namespace_modules[$module]['frontend'])
             ->group(base_path('Modules/Users/routes/frontend.php'));

        //Admin
        Route::prefix('admin/users')
             ->middleware('web')
             ->namespace($this->namespace_modules[$module]['admin'])
             ->group(base_path('Modules/Users/routes/admin.php'));
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes are only for admin.
     *
     * @return void
     */
    protected function mapRolesRoutes()
    {
        $module = 'roles';

        //Frontend
        Route::prefix('roles')
             ->middleware('web')
             ->namespace($this->namespace_modules[$module]['frontend'])
             ->group(base_path('Modules/Roles/routes/frontend.php'));

        //Admin
        Route::prefix('admin/roles')
             ->middleware('web')
             ->namespace($this->namespace_modules[$module]['admin'])
             ->group(base_path('Modules/Roles/routes/admin.php'));
    }
}
