/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./Components/Example.vue'));

import {
	randomString
} from './Helpers/randomString.helper';

// import Vuex from 'vuex';
// Vue.use(Vuex);

import store from './Store';

const app = new Vue({
	store,
	el: '#app',
	data: {
		items: [{
				id: 1,
				name: "Titulo 1"
			}, {
				id: 2,
				name: "Titulo 2"
			}, {
				id: 3,
				name: "Titulo 3"
			}, {
				id: 4,
				name: "Titulo 4"
			},

		]
	},
	mounted() {},
	methods: {
		discardItem(item) {
			var index = this.items.indexOf(item)
			this.items.splice(index, 1)
		}
	}
});