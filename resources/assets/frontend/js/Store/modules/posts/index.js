// import PostService from '../../../Services/PostService'
// let Post = new PostService();

const actions = {
	fetchPosts({
		commit
	}) {
		var counter = 0;
		// let posts_promise = Post.get();
		// posts_promise.then(Posts => {
		// 	commit('updatePostList', Posts);
		// })
	}
}

const getters = {

}

const mutations = {
	updatePostList(state, payload) {
		state.posts = payload;
	}
}

const state = {
	posts: []
}

const postModule = {
	namespaced: true,
	actions,
	getters,
	mutations,
	state
};


export default postModule