import UserService from '../../../Services/UserService'
let User = new UserService();

const actions = {
	fetchUsers({
		commit,
		state
	}) {

		var counter = 0
		var connection = undefined

		let req = () => {
			connection = setTimeout(getUsers, 1000)
		}

		var getUsers = () => {
			let users_promise = User.get();
			users_promise.then(users => {
				commit('users/updateUserList', users, {
					root: true
				});
				if (state.realtime) {
					req();
				}

			})
		}

		if (state.realtime) {
			req();
		} else {
			if (typeof connection != 'undefined') {
				clearTimeout(connection)
			}
			getUsers();
		}

	},
	setRealtime({
		commit,
		state
	}, payload) {
		console.log("stopping realtime")
		commit('users/setRealtime', payload, {
			root: true
		})
	}
}




export default actions;