const mutations = {
	updateUserList(state, payload) {
		state.users = payload.users.data;
	},
	setRealtime(state, payload) {
		state.realtime = payload;
	}
}

export default mutations;