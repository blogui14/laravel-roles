import actions from './actions.js';
import state from './state.js';
import getters from './getters.js';
import mutations from './mutations.js';

const module = {
	namespaced: true,
	actions,
	getters,
	mutations,
	state
};

export default module