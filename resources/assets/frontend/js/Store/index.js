import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import posts from './modules/posts'
import users from './modules/users'


const state = {
  count: 0,
  users: [],
  newList: []
}


const mutations = {
  INCREMENT(state) {
    state.count++
  },
  DECREMENT(state) {
    state.count--
  },
  updateUserList(state, payload) {
    state.users = payload;
  }
}

const actions = {
  incrementAsync({
    commit
  }) {
    setTimeout(() => {
      commit('INCREMENT')
    }, 200)
  },
  fetchUsers({
    commit
  }) {
    var counter = 0;
    let users_promise = User.get();
    users_promise.then(users => {
      commit('updateUserList', users);
    })
  }
}

let loadData = {
  state,
  mutations,
  actions
}

const store = new Vuex.Store({
  modules: {
    users,
    posts,
  }
})

export default store