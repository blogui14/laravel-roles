/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// $(document)
// 	.ready(function() {
// 		$('#name-field')
// 			.on('keydown keyup keypress', function(e) {
// 				var value = e.target.value;

// 				value = value.split(" ")
// 					.join(".");
// 				value = value.split("á")
// 					.join("a");
// 				value = value.split("é")
// 					.join("e");
// 				value = value.split("í")
// 					.join("i");
// 				value = value.split("ó")
// 					.join("o");
// 				value = value.split("ú")
// 					.join("u");
// 				value = value.toLowerCase();

// 				$("#slug-field")
// 					.val(value)

// 			});
// 	});

function randomString(length) {
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

	if (!length) {
		length = Math.floor(Math.random() * chars.length);
	}

	var str = '';
	for (var i = 0; i < length; i++) {
		str += chars[Math.floor(Math.random() * chars.length)];
	}
	return str;
}
const Bus = new Vue();
// Register a global custom directive called v-focus
// Vue.directive('dismissable', {
// 	// When the bound element is inserted into the DOM...
// 	bind: function(el, binding) {
// 		// Focus the element
// 		// Touch Event

// 		var swipeLeft = false;
// 		var swipeRight = false;
// 		var deleting = false;

// 		var id = randomString(10);
// 		var mc = new Hammer(el);
// 		mc.set({
// 				prevent_default: false
// 			})
// 			// Dismissible Collections
// 		mc.on('pan', function(e) {
// 				if (e.pointerType === "touch") {
// 					var $this = $(el);
// 					var direction = e.direction;
// 					var x = e.deltaX;
// 					var velocityX = e.velocityX;

// 					Velocity($this, {
// 							translateX: x
// 						}, {
// 							duration: 50,
// 							queue: false,
// 							easing: 'easeOutQuad'
// 						})
// 						// Swipe Left
// 					if (direction === 4 && (x > ($this.innerWidth() / 2) || velocityX < -0.75)) {
// 						swipeLeft = true;
// 					}

// 					// Swipe Right
// 					if (direction === 2 && (x < (-1 * $this.innerWidth() / 2) || velocityX > 0.75)) {
// 						swipeRight = true;
// 					}
// 				}
// 			})
// 			.on('panend', function(e) {
// 				// Reset if collection is moved back into original position

// 				var val = ($(el)
// 					.innerWidth() / 2)
// 				if (Math.abs(e.deltaX) < ($(el)
// 						.innerWidth() / 2)) {
// 					swipeRight = false;
// 					swipeLeft = false;
// 				}
// 				if (e.pointerType === "touch") {
// 					var $this = $(el);
// 					if (swipeLeft || swipeRight) {
// 						var fullWidth;
// 						if (swipeLeft) {
// 							fullWidth = $this.innerWidth();
// 						} else {
// 							fullWidth = -1 * $this.innerWidth();
// 						}

// 						Velocity($this, {
// 							translateX: fullWidth,
// 						}, {
// 							duration: 100,
// 							queue: false,
// 							easing: 'easeOutQuad',
// 							complete: function() {

// 								Velocity($this, {
// 									//height: 0,
// 									translateX: 0,
// 									padding: 'initial',
// 									//opacity: 0.2
// 								}, {
// 									duration: 200,
// 									queue: false,
// 									easing: 'easeOutQuad',
// 									complete: function() {

// 									}
// 								});

// 								var oldEl = _.cloneDeep($(el)
// 									.html());

// 								el.innerHTML =
// 									'<button class="btn btn-danger right" id="' + id + '"">' + 'Deshacer' + '</button>' +
// 									'</li>';

// 								$(el)
// 									.css('padding', '10px')
// 									.css('text-align', 'right')
// 									.addClass('deleting');


// 								var deleting = true;

// 								$('button#' + id)
// 									.on('click', function() {
// 										deleting = false;
// 										el.innerHTML = oldEl;
// 										$(el)
// 											.css('padding', '10px')
// 											.css('text-align', 'left')
// 											//.css('translateX', fullWidth * -1);

// 										Velocity(el, {
// 											//height: 0,
// 											translateX: fullWidth,
// 											//opacity: 0.2
// 										}, {
// 											duration: 0,
// 											queue: false,
// 											easing: 'easeOutQuad',
// 											complete: function() {

// 												Velocity(el, {
// 													//height: 0,
// 													translateX: 0,
// 													//opacity: 0.2
// 												}, {
// 													duration: 500,
// 													queue: false,
// 													easing: 'easeOutQuad',
// 													complete: function() {

// 													}
// 												});
// 											}
// 										});
// 									});

// 								setTimeout(function() {
// 									if (deleting) {
// 										$(el)
// 											.css('padding', '10px')
// 											.css('text-align', 'left')


// 										$(el)
// 											.fadeOut('fast', function() {
// 												Bus.$emit('dismissed-element', binding.value)
// 												$(this)
// 													.closest('ul')
// 													.find('li')
// 													.css('display', 'block')
// 													.removeClass('deleting');;

// 											})
// 									}
// 								}, 7000);
// 							}
// 						})
// 					} else {
// 						Velocity($this, {
// 							translateX: 0,
// 						}, {
// 							duration: 100,
// 							queue: false,
// 							easing: 'easeOutQuad'
// 						});
// 					}
// 					swipeLeft = false;
// 					swipeRight = false;

// 					$(el)
// 						.removeClass('deleting');
// 				}
// 			});

// 	}
// })


const app = new Vue({
	el: '#app',
	data: {
		items: [{
				id: 1,
				name: "Titulo 1"
			}, {
				id: 2,
				name: "Titulo 2"
			}, {
				id: 3,
				name: "Titulo 3"
			}, {
				id: 4,
				name: "Titulo 4"
			},

		]
	},
	mounted() {
		Bus.$on('dismissed-element', this.discardItem)
	},
	methods: {
		discardItem(item) {
			console.log('itemToDiscard', item)
			var index = this.items.indexOf(item)
			this.items.splice(index, 1)
		}
	}
});