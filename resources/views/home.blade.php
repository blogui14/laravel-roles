@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>

                <ul style="padding:0;margin:0">
                    <li  v-for="item in items" style="padding:10px;border:1px solid red" v-dismissable="item" v-text="item.name"></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
