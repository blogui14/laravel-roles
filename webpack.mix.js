const {
  mix
} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/admin-lte/js/app.js', 'public/admin-lte/js')
  .js('resources/assets/admin-lte/js/app-landing.js', 'public/admin-lte/js/app-landing.js')
    .sass('resources/assets/admin-lte/sass/app.scss', 'public/admin-lte/css')
    .less('node_modules/bootstrap-less/bootstrap/bootstrap.less', 'public/admin-lte/css/bootstrap.css')
    .less('resources/assets/admin-lte/less/admin-lte-app.less', 'public/admin-lte/css/admin-lte-app.css')
    .less('node_modules/toastr/toastr.less', 'public/admin-lte/css/toastr.css');
mix
  .combine([
    'public/admin-lte/css/app.css',
    'node_modules/admin-lte/dist/css/skins/_all-skins.css',
    'public/admin-lte/css/admin-lte-app.css',
    'node_modules/icheck/skins/square/blue.css',
    'public/admin-lte/css/toastr.css'
  ], 'public/admin-lte/css/all.css')
  .combine([
    'public/admin-lte/css/bootstrap.css',
    'resources/assets/admin-lte/css/main.css'
  ], 'public/admin-lte/css/all-landing.css')
  //APP RESOURCES
  // .copy('resources/assets/admin-lte/img/*.*', 'public/admin-lte/img')
  // //VENDOR RESOURCES
  // .copy('node_modules/font-awesome/fonts/*.*', 'public/admin-lte/fonts/')
  // .copy('node_modules/ionicons/dist/fonts/*.*', 'public/admin-lte/fonts/')
  // .copy('node_modules/admin-lte/bootstrap/fonts/*.*', 'public/admin-lte/fonts/bootstrap')
  // .copy('node_modules/admin-lte/dist/css/skins/*.*', 'public/admin-lte/css/skins')
  // .copy('node_modules/admin-lte/dist/img', 'public/admin-lte/img')
  // .copy('node_modules/admin-lte/plugins', 'public/admin-lte/plugins')
  // .copy('node_modules/icheck/skins/square/blue.png', 'public/admin-lte/css')
  // .copy('node_modules/icheck/skins/square/blue@2x.png', 'public/admin-lte/css');



//Frontend
mix.js('resources/assets/frontend/js/app.js', 'public/frontend/js')
  .sass('resources/assets/frontend/sass/app.scss', 'public/frontend/css');

